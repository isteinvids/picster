<?php
$msg = null;


/** Validate captcha */
if (isset($_POST['name']) && isset($_POST['adminpass'])) {
    if (empty($_REQUEST['captcha']) || empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) {
        echo "<script>alert('Invalid captcha');</script>";
        return;
    }
    unset($_SESSION['captcha']);

    $name = $_POST['name'];
    $pass = $_POST['adminpass'];

    function generateRandomString($db, $length = 5) {
        do {
                //0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            $checkunique = $db->query("SELECT * FROM messageboards WHERE boardid = '" . $randomString . "'");
            $checkunique->execute();
        } while ($checkunique->rowCount() > 0);
        return $randomString;
    }

    $randomstring = generateRandomString($db);
    $query = "INSERT INTO messageboards ( name, boardid, adminpass ) VALUES ('" . $name . "', '" . $randomstring . "', '" . $pass . "')";
    try {
            // Execute the query
        $stmt = $db->prepare($query);
        $result = $stmt->execute();
    } catch (PDOException $ex) {
        print_r($ex);
    }
    $finalurl = 'http://' . $_SERVER['HTTP_HOST'] . "/board?" . $randomstring;
}
