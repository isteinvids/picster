<?php
$msg = null;
if ($_POST) {
    if ($_FILES["file"]["error"] > 0) {
        if ($_FILES["file"]["error"] == UPLOAD_ERR_INI_SIZE) {
            $msg = "File exceeds maximum size";
        } else if ($_FILES["file"]["error"] == UPLOAD_ERR_NO_FILE) {
            $msg = "No file was uploaded";
        }
    } else {
        if (move_uploaded_file($_FILES["file"]["tmp_name"], "temp/" . $_FILES["file"]["name"])) {
            if (!(getimagesize("temp/" . $_FILES["file"]["name"]) == null || getimagesize("temp/" . $_FILES["file"]["name"]) == "")) {

                function generateRandomString($db, $length = 5) {
                    do {
                        //0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
                        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $randomString = '';
                        for ($i = 0; $i < $length; $i++) {
                            $randomString .= $characters[rand(0, strlen($characters) - 1)];
                        }
                        $checkunique = $db->query("SELECT * FROM images WHERE fileid = '" . $randomString . "'");
                        $checkunique->execute();
                    } while ($checkunique->rowCount() > 0);
                    return $randomString;
                }

                $username = "Guest";
                if (!empty($_SESSION['user'])) {
                    $username = $_SESSION['user']['username'];
                }
                $name = "";
                if (empty($_POST['name'])) {
                    $name = "Untitled";
                } else {
                    $name = $_POST['name'];
                }
                $randomstring = generateRandomString($db);
                $filename = "usrimgs/" . $randomstring . "." . pathinfo(("temp/" . $_FILES["file"]["name"]), PATHINFO_EXTENSION);
                rename("temp/" . $_FILES["file"]["name"], $filename);
        		$query = "INSERT INTO images ( name, date, filename, views, fileid, username ) VALUES ({$db->quote($name)}, NOW(), {$db->quote($filename)}, '0', {$db->quote($randomstring)}, {$db->quote($username)} )";
                try {
                    // Execute the query
                    $stmt = $db->prepare($query);
                    $result = $stmt->execute();
                } catch (PDOException $ex) {
                    print_r($ex);
                }
                echo "</br>" . $filename;
                header("Location: i?" . $randomstring);
                die();
            } else {
                unlink("temp/" . $_FILES["file"]["name"]);
                $msg = "File is not a valid image";
            }
        }
    }
}