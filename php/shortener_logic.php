<?php
$msg = null;
$finalurl = null;

function valid_url($str) {
    return (!preg_match('/^(http|https):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $str)) ? FALSE : TRUE;
}

//echo generateRandomString();
if (isset($_POST['url'])) {
    if (empty($_REQUEST['captcha']) || empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) {
        echo "<script>alert('Invalid captcha');</script>";
        return;
    }
    unset($_SESSION['captcha']);
    if (valid_url($_POST['url'])) {

        function generateRandomString($db, $length = 5) {
            do {
                //0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, strlen($characters) - 1)];
                }
                $checkunique = $db->query("SELECT * FROM urls WHERE urlid = '" . $randomString . "'");
                $checkunique->execute();
            } while ($checkunique->rowCount() > 0);
            return $randomString;
        }

        $randomstring = generateRandomString($db);
        $name = "Guest";
        if (!empty($_SESSION['user'])) {
            $name = $_SESSION['user']['username'];
        }
        $query = "INSERT INTO urls ( username, urlid, url, hits ) VALUES ('" . $name . "', '" . $randomstring . "', '" . $_POST['url'] . "', '0' )";
        try {
            // Execute the query
            $stmt = $db->prepare($query);
            $result = $stmt->execute();
        } catch (PDOException $ex) {
            print_r($ex);
        }
        $finalurl = 'http://' . $_SERVER['HTTP_HOST'] . "/url?" . $randomstring;
    } else {
        $msg = "Url is invalid";
    }
}
?>
