$(document).ready(function() {
  $('.image-link').magnificPopup({type:'image'});
  $('.image-animation').magnificPopup({ 
    type: 'image',
    mainClass: 'mfp-fade',
    removalDelay: 160
  });
  document.onclick = function(e) {
    e = e || window.event;
    var element = e.target || e.srcElement;

    if ((element.tagName === 'A' || element.tagName === 'a') && element.hasAttribute("inpage")) {
      $("#mainpage").html("<center>Loading page...<br/><br/><img src='loading.gif'/></center>");
      $(".active").removeClass("active");
      $("#mainpage").load(element.href + "?min", function() {
        $('.image-link').magnificPopup({type:'image'});
        $('.image-animation').magnificPopup({ 
          type: 'image',
          mainClass: 'mfp-fade',
          removalDelay: 160
        });
      });
      return false;
    }
  };
});

