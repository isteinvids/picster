<?php
include("common.php");
include("php/shortener_logic.php");

if (isset($msg)) {
    echo "<script>alert(\"" . $msg . "\")</script>";
}

$dotheuselessjunk = !isset($_GET["min"]);
if ($dotheuselessjunk) {
    echo file_get_contents("header.html");
}
?>
<h3><p>Shorten any URL here!</p></h3><br/>
<?php 
if (isset($finalurl)) {
    echo "Your new URL is: <a href=\"" . $finalurl . "\">" . $finalurl . "</a>";
} else {
?>
<form action="shortener" method="post"enctype="multipart/form-data">
    <label for="name">Long URL:</label><br/>
    <input class="form-control" style="width: 300px;" type="text" name="url" value="http://"><br/>
    <!-- captcha stuffs -->
    <label for="name">Write the following word:</label><br/>
    <img src="captcha/captcha.php" id="captcha" /><br/>
    <a href="#" onclick="
    document.getElementById('captcha').src = 'captcha/captcha.php?' + Math.random();
    document.getElementById('captcha-form').focus();"
    id="change-image">Not readable? Change text.</a><br/><br/>
    <input class="form-control" style="width: 300px;" type="text" name="captcha" id="captcha-form" autocomplete="off" /><br/><br/>
    
    <input class="btn btn-primary" type="submit" name="submit" value="Submit" />
</form>
<?php } ?>
<script>
    document.getElementById("nav_shortener").className = "active";
</script>
<?php
if ($dotheuselessjunk) {
    echo file_get_contents("footer.html");
}