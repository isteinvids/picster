<style media="screen" type="text/css">
    .imageFrame {
        overflow:hidden;
        width:100px;
        height:100px;
        padding:10px;
        margin-left:10px;
        margin-right:10px;
        margin-top:10px;
        margin-bottom:10px;
        border:7px solid #EEEEEE;
        position:relative;
    }

    .imageFrame img{
        position: absolute;
        top:50%;
        left:50%;
        margin-left: -50px;
        margin-top: -50px;
        display: block;
    }
</style><?php
require("common.php");
if (!empty($_SESSION['user']) && $_SESSION['user']['rights'] == "2") {

    $images = $db->query("SELECT * FROM images");
    $images->execute();
    for ($i = 0; $i < $images->rowCount(); $i++) {
        $cat = $images->fetch();
        echo "<center>";
        echo "<div class=\"imageFrame\">";
        echo "<a href=\"i?id=" . $cat['fileid'] . "\">";
        echo "<img src=\"" . $cat['filename'] . "\" alt=\"thumb\"  />";
        echo "</a>";
        echo "</div>";
        echo "</center>";
    }
}
?>
