<?php
include("common.php");
include("php/upload_logic.php");

if (isset($msg)) {
    echo "<script>alert(\"" . $msg . "\")</script>";
}

$dotheuselessjunk = !isset($_GET["min"]);
if ($dotheuselessjunk) {
    echo file_get_contents("header.html");
}
?>
<h3><p>Upload any image here!</p></h3><br/>
<form action="upload" method="post" enctype="multipart/form-data">
    <label for="name">Image name:</label><br/>
    <input class="form-control" style="width: 300px;" type="text" name="name" placeholder="Optional"/><br/>
    <label for="file">File:</label>
    <input class="form-control" type="file" name="file" id="file" style="width: 300px;"/><br/>
    <input class="btn btn-primary" type="submit" name="submit" value="Submit" />
</form>
<script>
    document.getElementById("nav_upload").className = "active";
</script>
<?php
if ($dotheuselessjunk) {
    echo file_get_contents("footer.html");
}