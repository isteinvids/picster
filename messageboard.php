<?php
include("common.php");
include("php/messageboard_logic.php");

if (isset($msg)) {
  echo "<script>alert(\"" . $msg . "\")</script>";
}

$dotheuselessjunk = !isset($_GET["min"]);
if ($dotheuselessjunk) {
  echo file_get_contents("header.html");
}
?>
<h3><p>Create an Embeddable Comment Area here!</p></h3><br/>
<?php 
if (isset($finalurl)) {
  echo "Your message board URL is: <a href=\"" . $finalurl . "\"  target=\"_blank\">" . $finalurl . "</a>, your embed HTML code is: </br>";
  echo "<div style=\"font-size: 12px; margin-top: 10px; margin-left: 10px; background-color: lightgray; width: 400px;\">";
  echo "<br/><p style=\"margin-left: 20px;\">&lt;embed src=\"" . $finalurl . "\" width=\"500\" height=\"600\"&gt;<br/><br/></p>";
  echo "</div>";
} else {
  ?>
  <form id="theform" action="messageboard" method="post" enctype="multipart/form-data">
    <!-- Workaround to disable Chrome's autofill. Chrome was great, once :( -->
    <input style="display:none" type="text" name="fakeusernameremembered"/>
    <input style="display:none" type="password" name="fakepasswordremembered"/>

    <label for="name">Name:</label><br/>
    <input class="form-control" style="width: 300px;" type="text" name="name" placeholder="Optional"><br/>
    <label for="name">Admin password:</label><br/>
    <input class="form-control" style="width: 300px;" type="password" name="adminpass" placeholder="Password" required><br/>
    <!-- captcha stuffs -->
    <label for="name">Write the following word:</label><br/>
    <img src="captcha/captcha.php" id="captcha" /><br/>
    <a href="#" onclick="
    document.getElementById('captcha').src = 'captcha/captcha.php?' + Math.random();
    document.getElementById('captcha-form').focus();"
    id="change-image">Not readable? Change text.</a><br/><br/>
    <input class="form-control" style="width: 300px;" type="text" name="captcha" id="captcha-form" autocomplete="off" /><br/><br/>

    <input class="btn btn-primary" type="submit" name="submit" value="Submit" />
  </form>
  <br/><br/>
  <center>
    <b>Example of a Comment Area:</b><br/><br/>
    <embed src="board?default" width="500" height="600">
  </center>

  <?php } ?>
  <script>
    document.getElementById("nav_messageboard").className = "active";
  </script>
  <?php
  if ($dotheuselessjunk) {
    echo file_get_contents("footer.html");
  }