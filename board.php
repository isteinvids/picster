<?php 
require("common.php");

$id = "";
$pass = "";
$adminview = false;
if (count($_GET) !== 1) {
  die("invalid argument</font></body>");
}
foreach ($_GET as $key => $value) {
  $pass = $value;
  $id = $key;
}
$boards = $db->query("SELECT * FROM messageboards WHERE boardid = '" . $id . "'");
$boards->execute();
$cat = $boards->fetch();
if ($boards->rowCount() < 1) {
  die("Invalid board</font></body>");
}
if ($cat["adminpass"] === $pass) {
  $adminview = true;
}
$post = file_get_contents('php://input');
if (isset($post) && !empty($post)) {
  if ($post === "refresh") {
    //loop here
    $boards = $db->query("SELECT * FROM boardcomments WHERE boardid = '" . $cat["id"] . "' AND deleted = '0'");
    $boards->execute();
    for ($i = 0; $i < $boards->rowCount(); $i++) {
      $post = $boards->fetch();
      ?>
      <br/>
      <div style="margin-left: 20px; border-style: solid; border-width: 1px; width: calc(100% - 40px);">
        <div style="margin-left: 10px; margin-top: 5px;">
          <?php echo empty($post["username"]) ? "Anonymous" : $post["username"]; ?>:
        </div>
        <div style="margin-left: 100px; overflow: auto; margin-bottom: 15px; margin-top: 5px;">
          <?php echo str_replace("\n", "<br/>", $post["comment"]); ?>
        </div>
      </div>
      <?php
    } 
    die(); 
  }
  $thejson = json_decode($post, true);

  if (empty($thejson['captcha']) || empty($_SESSION['board_captcha']) || trim(strtolower($thejson['captcha'])) != $_SESSION['board_captcha']) {
    echo "Invalid captcha";
    return;
  }
  unset($_SESSION['board_captcha']);

  $name = $thejson["name"];
  $comment = $thejson["comment"];

  $query = "INSERT INTO boardcomments ( username, timestamp, comment, boardid, deleted) VALUES ('" . $name . "', '" . time() . "', '" . $comment. "', '" . $cat["id"] . "', '0' )";
  try {
            // Execute the query
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
  } catch (PDOException $ex) {
    print_r($ex);
  }
  die("200");
}
?>
<body style="background-color: lightgray;">
  <font style="font-family: sans-serif;color: black; text-decoration: none;">
    <?php

    $images = $db->query("SELECT * FROM messageboards WHERE boardid = '" . $id . "'");
    $images->execute();
    $cat = $images->fetch();
    if ($images->rowCount() < 1) {
      die("Invalid id</font></body>");
    }
    if (empty($cat["name"])) {
      echo "Untitled";
    } else {
      echo $cat["name"];
    }
    echo " - <a href=\"#\" onclick=\"return onrefresh();\">Refresh</a>";
    if ($adminview) {
      echo " - Admin";
    }
    ?>

    <div id="comments">
    </div>

    <br/><br/>
    post a comment:<br/><br/>
    <form id="theform" onsubmit="return handleComment(this);" method="post" enctype="multipart/form-data">
      <label for="name">Name:</label><br/>
      <input class="form-control" style="width: 300px;" type="text" name="name"><br/><br/>
      <label for="name">Comment:</label><br/>
      <textarea rows="5" cols="40" name="comment" required></textarea><br/><br/>

      <label for="name">Write the following word:</label><br/>
      <img src="captcha/board_captcha.php" id="captcha" /><br/>
      <a href="#" onclick="
      document.getElementById('captcha').src = 'captcha/board_captcha.php?' + Math.random();
      document.getElementById('captcha-form').focus();"
      id="change-image">Not readable? Change text.</a><br/><br/>
      <input class="form-control" style="width: 300px;" type="text" name="captcha" id="captcha-form" autocomplete="off" /><br/><br/>

      <input class="btn btn-primary" type="submit" name="submit" value="Submit" />
    </form>
    <script src="./js/jquery-1.9.1.min.js"></script>
    <script>
      refreshComments();

      function onrefresh() {
        refreshComments();
        return false;
      }

      function refreshComments() {
        $.ajax({
          url: "board.php?<?php echo $id; ?>",
          type: 'POST',
          data: "refresh",
          error: function(jqXHR, textStatus, errorThrown) {
            //???
          },
          success: function(result) {
            $("#comments").html(result);
          }
        });
      }

      function handleComment(theform) {
        fname = theform.name.value;
        fcomment = theform.comment.value;
        fcaptcha = theform.captcha.value;

        theform.captcha.value = "";

        $.ajax({
          url: "board.php?<?php echo $id; ?>",
          type: 'POST',
          data: JSON.stringify({"name": fname, "comment": fcomment, "captcha": fcaptcha}),
          error: function(jqXHR, textStatus, errorThrown) {
            document.getElementById('captcha').src = 'captcha/board_captcha.php?' + Math.random();
            alert("Could not submit comment");
          },
          success: function(result) {
            document.getElementById('captcha').src = 'captcha/board_captcha.php?' + Math.random();
            if (result !== "200") {
              alert(result);
            } else {
              theform.comment.value = "";
            }
            refreshComments();
          }
        });
        return false;
      }
    </script>
  </font>
</body>