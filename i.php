<?php 
$id = "";
if (count($_GET) >= 1) {
    foreach ($_GET as $key => $value) {
        if ($value === "" && $key !== 'thumb') {
            $id = $key;
        }
    }
}
require("common.php");

function startsWith($haystack, $needle) {
	return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function endsWith($haystack, $needle) {
    return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
}

if (endsWith($id, "!") || endsWith($id, "?") || startsWith($id, "$") || isset($_GET['thumb'])) {
	if(startsWith($id, "$")){
    	$id = substr($id, 1, strlen($id));
	} else if (endsWith($id, "!") || endsWith($id, "?")) {
    	$id = substr($id, 0, strlen($id) - 1);
	}

    $images = $db->query("SELECT * FROM images WHERE fileid = '" . $id . "'");
    $images->execute();
    if ($images->rowCount() < 1) {
        die("Invalid image");
    }
    $cat = $images->fetch();

    $file_extension = substr($cat['filename'], strlen($cat['filename'])-3, strlen($cat['filename']));
    switch( exif_imagetype($cat['filename']) ) {
        case IMAGETYPE_GIF: $file_extension='gif'; $ctype="image/gif"; break;
        case IMAGETYPE_PNG: $file_extension='png'; $ctype="image/png"; break;
        case IMAGETYPE_JPEG: $file_extension='jpeg'; $ctype="image/jpg"; break;
        default: die('invalid image type');
    }
    header('Content-type: ' . $ctype);
    if(isset($_GET['thumb'])){
    	$img = resize_image($cat['filename'], 164, 164, $file_extension);
		imagepng($img);
		imagedestroy($img);
    } else {
    	die(file_get_contents($cat['filename']));
    }
    die();
}
?><head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Picster</title>
</head>
<link rel="stylesheet" href="./css/pad.min.css" type="text/css"/>
<link rel="stylesheet" href="css/main.css" type="text/css"/>
<link rel="stylesheet" href="css/magnific-popup.css"> 
<script src="./js/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
    function func(img) {
        document.getElementById("loadingtext").hidden = true;
        document.getElementById("imgL").hidden = false;
        if (img.height > 700 || img.width > 700) {
            if (img.height >= img.width) {
                img.style.height = "100%";
                img.style.width = "auto";
            } else {
                img.style.width = "100%";
                img.style.height = "auto";
            }
        }
    }
    function rsz(imgname) {
        alert('resize ' + imgname);
    }
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=210338402455607";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div id="fb-root"></div>
<body style="background-color:black;">
    <font style="font-family: sans-serif;color: white; text-decoration: none;"><?php
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $image_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "!";
        if (!empty($id)) {
            $images = $db->query("SELECT * FROM images WHERE fileid = '" . $id . "'");
            $images->execute();
            $cat = $images->fetch();
            if ($images->rowCount() < 1) {
                die("Invalid image");
            }
            echo "<h1 align=\"left\"><b><a href=\"index\" style=\"font-family: sans-serif;color: white;text-decoration: none;\" >Picster</a></b></h1>";
            echo "<h2 align=\"center\"><b>" . $cat['name'] . "</b></h2>";
            ?>
            <table width="800" height="700" cellpadding="0" cellspacing="5" border="1" style="margin-left:auto;margin-right:auto;">
                <tr>
                    <td width="50%" height="80%"id="td1" style='text-align: center;'>
                        <span id="loadingtext" style="color: white;">Loading image...</span>
                        <a hidden id="imgL" class="image-animation" href="<?php echo $image_link; ?>">
                            <img onload="func(this);" src="<?php echo $image_link; ?>"  />
                        </a>
                    </td>
                </tr>
            </table>

            <?php
            if ((!empty($_SESSION['user']) && $_SESSION['user']['username'] != $cat['username']) || empty($_SESSION['user'])) {
                try {
                    $result = $db->query("UPDATE images SET views = '" . (intval($cat['views']) + 1) . "' WHERE id='" . $cat['id'] . "'");
                    $result->execute();
                } catch (PDOException $ex) {
                    echo "Failed to connect to database :(";
                }
            }
            echo "<h3 align=\"right\">" . ($cat['views'] + 1) . " views</h3>";
            echo "<h5 align=\"left\">Uploaded by <a href=\"profile?user=" . $cat['username'] . "\" style=\"font-family: sans-serif;color: white;text-decoration: none;\" >" . $cat['username'] . "</a></h5>";
            ?>
            <div class="fb-like" data-href="<?php echo $actual_link; ?>" data-width="450" data-show-faces="true" data-send="true"></div>
            <br/>
            <a href="https://twitter.com/share" class="twitter-share-button" data-text="I've just shared an image!" data-hashtags="SharedOnPicster">Tweet</a>
            <script>!function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + '://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');</script>
            <?php
        } else {
            die("Invalid image");
        }
        ?>
    </font>
    <script src="./js/main.js"></script>
    <script src="./js/magnific-popup.min.js"></script>
</body>