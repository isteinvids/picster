<?php

require("common.php");
$target_path = "uploads/";
$target_path = $target_path . basename($_FILES['uploadedfile']['name']);
if (move_uploaded_file($_FILES["uploadedfile"]["tmp_name"], "temp/" . $_FILES["uploadedfile"]["name"])) {
    if (!(getimagesize("temp/" . $_FILES["uploadedfile"]["name"]) == null || getimagesize("temp/" . $_FILES["uploadedfile"]["name"]) == "")) {

        function generateRandomString($db, $length = 15) {
            do {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, strlen($characters) - 1)];
                }
                $checkunique = $db->query("SELECT * FROM images WHERE fileid = '" . $randomString . "'");
                $checkunique->execute();
            } while ($checkunique->rowCount() > 0);
            return $randomString;
        }

        $name = "Untitled";
        if (!empty($_GET['name'])) {
            $name = $_GET['name'];
        }
        $username = "Guest";
        $randomstring = generateRandomString($db);
        $filename = "usrimgs/" . $randomstring;
        rename("temp/" . $_FILES["uploadedfile"]["name"], $filename);
        $query = "INSERT INTO images ( name, date, filename, views, fileid, username ) VALUES ({$db->quote($name)}, NOW(), {$db->quote($filename)}, '0', {$db->quote($randomstring)}, {$db->quote($username)} )";
        try {
            // Execute the query
            $stmt = $db->prepare($query);
            $result = $stmt->execute();
        } catch (PDOException $ex) {
            print_r($ex);
        }
        echo 'http://' . $_SERVER['HTTP_HOST'] . "/i?" . $randomstring;
    } else {
        unlink("temp/" . $_FILES["uploadedfile"]["name"]);
        echo "File is not a valid image!";
    }
}
// */
?>